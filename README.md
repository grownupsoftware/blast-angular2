# Blast-Angular2

![Alt text](./resources/images/b-circle-trans-100.png)
**with**
![Alt text](./resources/images/angular.png)

    
# Overview
This is a websocket client for Angular2 projects for connection to a *Blast Server*. It is a simple *wrapper* around the default 'WebSocket' implementation, offering additional features like 'reconnect' and the ability to queue messages before connect.

    
# Getting Started

```
    npm install blast-angular2
```

# Usage
## Connect
```
const blastService: BlastService = new BlastService('ws://127.0.0.1:8081/blast');
```
### Connection Options
```
 new BlastService(URL,[AutoConnect],[Protocols],[BlastConfig]);
```
| Parameter|Type| |Description |
|---|---|---|---|
| URL | string | Mandatory |URL with sever port and endpoint (default Blast Server is '/blast' - but this is configurable server side 
| AutoConnect | boolean | Optional (default is 'true')| - if true, establish a connection immediately, if 'false' then a call to *connect() is required 
| Protocols | string[] | Optional (default empty) |Sebsocket protocols 
| BlastConfig | BlastServiceConfig | Optional (default is {initialTimeout: 500, maxTimeout: 300000, reconnectIfNotNormalClose: true})|Set following: 'initialTimeout','maxTimeout','reconnectIfNotNormalClose' 

## Commands
| Command|Description |
|---|---|
| connect | Establish a connection (Only required if AutoConnect in constructor is manually set to false)
| close | Manually close the connection


## Callbacks
| CallBack|Description |
|---|---|
| OnOpen | When a connection is established 
| OnClose | When a connection is lost 
| OnError | When there is an error
| OnMessage | When a message is received 

*Example*

```
this.blastService.onMessage((msg: any) => {
    // we received a message
    console.log('message',msg.data);
});
```
### Observer
Monitor inbound messages via an 'Observer'

```
this.blastService.getDataStream().subscribe(
    (msg)=> {
        console.log("next", msg.data);
    },
    (msg)=> {
        console.log("error", msg);
    },
    ()=> {
        console.log("complete");
    }
);

// To send a message
blastService.send("Hello World");
```

## Development 
If modifying the core source code then:

```
   npm run setup
```

### NPM Commands   
| Command|Description |
|---|---|
| npm run lint | runs lint
| npm run compile | compiles tyepscript
| npm run minify | minifies the output javascript
| npm run bundle | includes any 3rd party libraries
| npm run bundle-minify | minifies the bundle
| npm run prepublish | runs all above commands
| npm run dev | lint,compile and npm link

Note: Although the main objective for this project is a 'npm module' it also serves as the *build* for our native javascript library.

to build ..
npm run packagr
npm publish dist

