import {BlastService, BLAST_VERSION} from '../src/blast-angular2';


describe('blast-angular2', () => {

    it('should be the correct version', () => {
        const blastService: BlastService = new BlastService('ws://127.0.0.1:8081/blast', false);

        expect(blastService.getVersion()).toEqual(BLAST_VERSION);
    });
});

