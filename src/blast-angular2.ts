import {Injectable, Optional} from '@angular/core';
/**
 * blast-angular2
 */
import {BlastHelpers} from './blast-helpers';
import {Subject} from 'rxjs/index';

export const BLAST_VERSION = '0.0.1';

export const LOG_LEVEL = {
    DEBUG: 0,
    INFO: 1,
    WARN: 2,
    ERROR: 3
};

export interface BlastServiceConfig {
    initialTimeout: number;
    maxTimeout: number;
    reconnectIfNotNormalClose: boolean;
}

//@Injectable()
export class BlastService {

    private reconnectAttempts = 0;
    private sendQueue: any[] = [];
    private onMessageCallbacks: any[] = [];
    private onOpenCallbacks: Function[] = [];
    private onErrorCallbacks: Function[] = [];
    private onCloseCallbacks: Function[] = [];
    private readyStateConstants = {
        'CONNECTING': 0,
        'OPEN': 1,
        'CLOSING': 2,
        'CLOSED': 3,
        'RECONNECT_ABORTED': 4
    };

    private normalCloseCode = 1000;
    private reconnectableStatusCodes = [4000];
    private socket: WebSocket;
    private dataStream: Subject<any>;
    private internalConnectionState: number;

    public logLevel = LOG_LEVEL.ERROR;

    constructor(private url: string,
        connectNow?: boolean,
        private protocols?: Array<string>,
        private config?: BlastServiceConfig
    ) {

        const match = new RegExp('wss?:\/\/').test(url);
        if (!match) {
            throw new Error('Invalid url provided');
        }
        this.config = config || {initialTimeout: 500, maxTimeout: 300000, reconnectIfNotNormalClose: true};
        this.dataStream = new Subject();

        if (connectNow === undefined || connectNow) {
            this.connect(true);
        }
    }

    connect(force = false) {
        const self = this;
        if (force || !this.socket || this.socket.readyState !== this.readyStateConstants.OPEN) {
            self.socket = this.protocols ? new WebSocket(this.url, this.protocols) : new WebSocket(this.url);

            self.socket.onopen = (ev: Event) => {
                this.onOpenHandler(ev);
            };
            self.socket.onmessage = (ev: MessageEvent) => {

                if (BlastHelpers.isJson(ev.data)) {
                    const message = JSON.parse(ev.data);
                    this.debug('BlastService', 'jsonMessage', 'passing to handlers', ev.data);

                    // call a json message handler - if true, then message handled mustn't carry on
                    if (self.handleJsonMessage(message) === true) {
                        return;
                    }
                }
                self.onMessageHandler(ev.data);
                this.dataStream.next(ev.data);
            };

            this.socket.onclose = (ev: CloseEvent) => {
                self.onCloseHandler(ev);
            };

            this.socket.onerror = (ev: ErrorEvent) => {
                self.onErrorHandler(ev);
                this.dataStream.error(ev);
            };

        }
    }

    /**
     * Run in Block Mode
     * Return true when can send and false in socket closed
     * @param data
     * @returns 
     */
    private sendMessage(data: any, binary?: boolean): boolean {
        const self = this;
        if (this.getReadyState() !== this.readyStateConstants.OPEN
            && this.getReadyState() !== this.readyStateConstants.CONNECTING) {
            this.connect();
        }
        this.debug('BlastService', 'sendMessage', data);
        self.sendQueue.push({message: data, binary: binary});
        if (self.socket.readyState === self.readyStateConstants.OPEN) {
            self.fireQueue();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Use {mode} mode to send {data} data
     * If no specify, Default SendMode is Observable mode
     * @param data
     * @param mode
     * @param binary
     * @returns 
     */
    send(data: any, binary?: boolean): any {
        return this.sendMessage(data, binary);
    }

    getDataStream(): Subject<any> {
        return this.dataStream;
    }


    notifyOpenCallbacks(event: Event) {
        for (let i = 0; i < this.onOpenCallbacks.length; i++) {
            this.onOpenCallbacks[i].call(this, event);
        }
    }

    fireQueue() {
        while (this.sendQueue.length && this.socket.readyState === this.readyStateConstants.OPEN) {
            const data = this.sendQueue.shift();

            if (data.binary) {
                this.socket.send(data.message);
            } else {
                this.socket.send(
                    BlastHelpers.isString(data.message) ? data.message : JSON.stringify(data.message)
                );
            }
        }
    }

    notifyCloseCallbacks(event: Event) {
        for (let i = 0; i < this.onCloseCallbacks.length; i++) {
            this.onCloseCallbacks[i].call(this, event);
        }
    }

    notifyErrorCallbacks(event: Event) {
        for (let i = 0; i < this.onErrorCallbacks.length; i++) {
            this.onErrorCallbacks[i].call(this, event);
        }
    }

    onOpen(cb: Function) {
        this.onOpenCallbacks.push(cb);
        return this;
    };

    onClose(cb: Function) {
        this.onCloseCallbacks.push(cb);
        return this;
    }

    onError(cb: Function) {
        this.onErrorCallbacks.push(cb);
        return this;
    };

    onMessage(callback: Function, options?) {
        if (!BlastHelpers.isFunction(callback)) {
            throw new Error('Callback must be a function');
        }

        this.onMessageCallbacks.push({
            fn: callback,
            pattern: options ? options.filter : undefined,
            autoApply: options ? options.autoApply : true
        });
        return this;
    }

    handleJsonMessage(message: any): boolean {
        // as a default return false i.e. don't change message flow
        // enables extended classes to override this function
        return false;
    }

    onMessageHandler(message: MessageEvent) {
        this.debug('BlastService', 'onMessageHandler', message.data);

        const self = this;
        let currentCallback;
        for (let i = 0; i < self.onMessageCallbacks.length; i++) {
            currentCallback = self.onMessageCallbacks[i];
            currentCallback.fn.apply(self, [message]);
        }
    };

    onOpenHandler(event: Event) {
        this.debug('BlastService', 'connected');
        this.reconnectAttempts = 0;
        this.notifyOpenCallbacks(event);
        this.fireQueue();
    }

    onCloseHandler(event: CloseEvent) {
        this.debug('BlastService', 'closed');
        this.notifyCloseCallbacks(event);
        if ((this.config.reconnectIfNotNormalClose && event.code !== this.normalCloseCode)
            || this.reconnectableStatusCodes.indexOf(event.code) > -1) {
            this.reconnect();
        } else {
            this.sendQueue = [];
            this.dataStream.complete();
        }
    };

    onErrorHandler(event: ErrorEvent) {
        this.debug('BlastService', 'onErrorHandler', event);
        this.notifyErrorCallbacks(event);
    };

    reconnect() {
        this.close(true);
        const backoffDelay = this.getBackoffDelay(++this.reconnectAttempts);
        //         let backoffDelaySeconds = backoffDelay / 1000;
        //         // console.log('Reconnecting in ' + backoffDelaySeconds + ' seconds');
        this.debug('BlastService', 'reconnectDelay', backoffDelay);
        setTimeout(() => this.connect(), backoffDelay);
        return this;
    }

    close(force: boolean = false) {
        if (force || !this.socket.bufferedAmount) {
            this.socket.close(this.normalCloseCode);
        }
        return this;
    };

    getBackoffDelay(attempt: number) {
        const R = Math.random() + 1;
        const T = this.config.initialTimeout;
        const F = 2;
        const N = attempt;
        const M = this.config.maxTimeout;

        return Math.floor(Math.min(R * T * Math.pow(F, N), M));
    };

    setInternalState(state: number) {
        if (Math.floor(state) !== state || state < 0 || state > 4) {
            throw new Error('state must be an integer between 0 and 4, got: ' + state);
        }
        this.internalConnectionState = state;
    }

    /**
     * Could be -1 if not initzialized yet
     * @returns 
     */
    getReadyState(): number {
        if (this.socket == null) {
            return -1;
        }
        return this.internalConnectionState || this.socket.readyState;
    }

    getVersion(): string {
        return BLAST_VERSION;
    }


    private hasConsole(): boolean {
        if (console === undefined) {
            return false;
        }
        return true;
    }
    private debug(...args: any[]) {
        if (this.hasConsole() && this.logLevel < 1) {
            console.debug.apply(console, args);
        }
    }

    info(...args: any[]) {
        if (this.hasConsole() && this.logLevel < 2) {
            console.debug.apply(console, args);
        }
    }
    warn(...args: any[]) {
        if (this.hasConsole() && this.logLevel < 4) {
            console.debug.apply(console, args);
        }
    }
    error(...args: any[]) {
        console.error.apply(console, args);
    }

    setLogLevel(level: number) {
        this.logLevel = level;
    }
}

