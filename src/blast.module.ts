import {NgModule} from '@angular/core';
import {BlastHelpers} from './blast-helpers';
import {BlastException} from './blast-exception';
import {BlastService} from './blast-angular2'

@NgModule({
    declarations: [
    ],
    imports: [
    ],
    providers: [
    //BlastService
    ]
    ,
    bootstrap: [],
    exports: [
//        BlastService,
//        BlastException,
//        BlastHelpers
    ]
})
export class BlastAngularModule {}
